package c2_3.seguridad.and802.myapplication.database

import android.content.ContentValues
import c2_3.seguridad.and802.myapplication.modelo.Persona

interface IPersona{
    fun guardar(persona:Persona): Long
    fun guardar(values:ContentValues): Long
    fun listar():ArrayList<Persona>
}