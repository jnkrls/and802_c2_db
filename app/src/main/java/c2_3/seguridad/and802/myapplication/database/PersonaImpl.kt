package c2_3.seguridad.and802.myapplication.database

import android.content.ContentValues
import android.content.Context
import c2_3.seguridad.and802.myapplication.modelo.Persona

class PersonaImpl(
    context:Context
) : IPersona{

    var helper = DatabaseHelper(context)

    override fun guardar(persona: Persona):Long {
        val db = helper.writableDatabase
        val values = ContentValues()
        values.put("nombre", persona.nombre)
        values.put("apellido", persona.apellido)
        values.put("correo", persona.correo)
        values.put("numero", persona.numero)

        return db.insert("persona", null, values)
    }

    override fun guardar(values:ContentValues):Long {
        val db = helper.writableDatabase
        return db.insert("persona", null, values)
    }


    override fun listar(): ArrayList<Persona> {
        val lista = ArrayList<Persona>()

        val db=helper.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM persona", null)

        if(cursor.moveToFirst()){
            while(cursor.moveToNext()){
                val id = cursor.getInt(cursor.getColumnIndex("id"))
                val nombre = cursor.getString(cursor.getColumnIndex("nombre"))
                val apellido = cursor.getString(cursor.getColumnIndex("apellido"))
                val correo = cursor.getString(cursor.getColumnIndex("correo"))
                val numero = cursor.getString(cursor.getColumnIndex("numero"))

                lista.add(Persona(id,nombre,apellido,correo,numero))
            }
        }

        return lista
    }

}