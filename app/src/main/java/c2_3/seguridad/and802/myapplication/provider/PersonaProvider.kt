package c2_3.seguridad.and802.myapplication.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import c2_3.seguridad.and802.myapplication.Constantes
import c2_3.seguridad.and802.myapplication.database.DatabaseHelper
import c2_3.seguridad.and802.myapplication.database.PersonaImpl
import c2_3.seguridad.and802.myapplication.modelo.Persona

class PersonaProvider:ContentProvider(){
    var uriMatch = UriMatcher(UriMatcher.NO_MATCH)
    var map  = mutableMapOf<String,String>()

    init {
        uriMatch.addURI(Constantes.AUTORIZACION, "t1", 1)
        map.put(Constantes.ID, Constantes.ID)
        map.put(Constantes.NOMBRE, Constantes.NOMBRE)
        map.put(Constantes.APELLIDO, Constantes.APELLIDO)
        map.put(Constantes.CORREO, Constantes.CORREO)
        map.put(Constantes.NUMERO, Constantes.NUMERO)
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val db = PersonaImpl(context)
        val id = db.guardar(values!!)
        val uri = ContentUris.withAppendedId(Constantes.URL,id)

        context.contentResolver.notifyChange(uri,null)

        return uri

    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?): Cursor?
    {
        val db = DatabaseHelper(context)
        val database = db.readableDatabase

        val cursor = database.rawQuery("SELECT * FROM persona",null)
        cursor.setNotificationUri(context.contentResolver,uri)

        return cursor

    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        return 1
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return 1
    }

    override fun getType(uri: Uri): String? {
        return Constantes.CONTENT_TYPE
    }


}