package c2_3.seguridad.and802.myapplication.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(context:Context):SQLiteOpenHelper(context, "clase_db", null, 1){

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL(
            "CREATE TABLE persona (" +
                    "id       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "nombre   TEXT, " +
                    "apellido TEXT, " +
                    "correo   TEXT, " +
                    "numero   TEXT" +
                ");"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}