package c2_3.seguridad.and802.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import c2_3.seguridad.and802.myapplication.R
import c2_3.seguridad.and802.myapplication.modelo.Persona
import kotlinx.android.synthetic.main.item_persona.view.*

class PersonaAdapter : BaseAdapter(){
    var lista = ArrayList<Persona>()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(parent!!.context)
            .inflate(R.layout.item_persona, parent, false)

        //Obtenemos los datos a mostrar
        val datos = getItem(position) as Persona
        view.tvNombre.text = datos.nombre
        view.tvApellido.text = datos.apellido
        view.tvCorreo.text = datos.correo
        view.tvNumero.text = datos.numero

        return view
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return lista[position].id.toLong()
    }

    override fun getCount(): Int {
        return lista.size
    }

    fun agregarDatos(datos:ArrayList<Persona>){
        lista.clear()
        lista.addAll(datos)
        notifyDataSetChanged()

    }

}