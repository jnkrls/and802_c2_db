package c2_3.seguridad.and802.myapplication.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import c2_3.seguridad.and802.myapplication.R
import c2_3.seguridad.and802.myapplication.adapter.PersonaAdapter
import c2_3.seguridad.and802.myapplication.database.PersonaImpl
import kotlinx.android.synthetic.main.activity_listado.*

class ListadoActivity : AppCompatActivity() {

    var adapter:PersonaAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listado)

        adapter = PersonaAdapter()
        lvDatos.adapter = adapter

        val bd = PersonaImpl(this)
        adapter!!.agregarDatos(bd.listar())

    }

}
