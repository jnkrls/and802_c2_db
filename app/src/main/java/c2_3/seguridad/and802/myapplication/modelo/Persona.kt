package c2_3.seguridad.and802.myapplication.modelo

data class Persona(
    var id: Int,
    var nombre: String,
    var apellido: String,
    var correo: String,
    var numero: String
)