package c2_3.seguridad.and802.myapplication

import android.net.Uri

class Constantes{
    companion object {
        val AUTORIZACION    = "com.google.clase02_3.provider"
        val URL             = Uri.parse("content://$AUTORIZACION/t1")
        val CONTENT_TYPE    = "vnd.android.cursor.dir/vnd.android.contentproviderclass.t1"

        val ID              = "id"
        val NOMBRE          = "nombre"
        val APELLIDO        = "apellido"
        val CORREO          = "correo"
        val NUMERO          = "numero"
    }
}