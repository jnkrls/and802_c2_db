package c2_3.seguridad.and802.myapplication.activity

import android.content.ContentValues
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import c2_3.seguridad.and802.myapplication.Constantes
import c2_3.seguridad.and802.myapplication.R
import c2_3.seguridad.and802.myapplication.database.PersonaImpl
import c2_3.seguridad.and802.myapplication.modelo.Persona
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()

        btnGuardar.setOnClickListener{
            val nombre = etNombre.text.toString()
            val apellido = etApellido.text.toString()
            val correo = etCorreo.text.toString()
            val numero = etNumero.text.toString()

            val persona = Persona(0, nombre, apellido,correo, numero)
            val db = PersonaImpl(this)
            db.guardar(persona)

            Toast.makeText(this, "Se guardo correctamente", Toast.LENGTH_SHORT).show()
        }

        btnMostrar.setOnClickListener{
            val intent = Intent(this, ListadoActivity::class.java)
            startActivity(intent)
        }

        btnProvider.setOnClickListener {
            val nombre = etNombre.text.toString()
            val apellido = etApellido.text.toString()
            val correo = etCorreo.text.toString()
            val numero = etNumero.text.toString()

            val persona = Persona(0, nombre, apellido,correo, numero)

            val values = ContentValues()
            values.put("nombre", persona.nombre)
            values.put("apellido", persona.apellido)
            values.put("correo", persona.correo)
            values.put("numero", persona.numero)
            contentResolver.insert(Constantes.URL,values)

            Toast.makeText(this, "Se guardo correctamente por provider", Toast.LENGTH_SHORT).show()
        }
    }
}
